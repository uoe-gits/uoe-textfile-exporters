import argparse
import subprocess
from pathlib import Path
import sys

from .common import Metrics
from .common import uoeparser


class MvCLI(Metrics):
    name = "node_mvcli"
    metrics = {'error': ('mvcli controller shows error', 'gauge')}


def main():
    parser = argparse.ArgumentParser(
        description='check status of raid card', parents=[uoeparser])
    parser.add_argument('-p', '--program', type=Path, metavar='PATH',
                        default="/usr/bin/mvcli",
                        help="name of mvcli binary")
    args = parser.parse_args()

    metrics = MvCLI(args)

    if not args.program.exists():
        metrics.log.error(f'binary {args.program} does not exist')
        sys.exit(1)

    try:
        cmd = subprocess.run([args.program, "info", "-o", "vd"],
                             text=True, capture_output=True, check=True)
    except subprocess.CalledProcessError as e:
        metrics.log.error(e)
        sys.exit(1)

    for line in cmd.stdout.split('\n'):
        try:
            key, value = line.split(':')
        except ValueError:
            continue
        key = key.strip()
        value = value.strip()

        if key == 'id':
            controller = value
        if key == 'status':
            if value == 'functional':
                value = 0
            else:
                value = 1
            metrics.add('error', value, controller=controller)

    metrics.close()


if __name__ == '__main__':
    main()
