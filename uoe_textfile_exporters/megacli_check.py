import argparse
import subprocess
from pathlib import Path
import sys

from .common import Metrics
from .common import uoeparser


class MegaCLI(Metrics):
    name = "node_megacli"
    metrics = {'media_error': ('media errors', 'counter'),
               'other_error': ('other errors', 'counter'),
               'predictive_failure': ('predictive failure', 'counter'),
               'firmware': ('firmware error state', 'gauge'),
               'smart': ('SMART errors are flagged', 'gauge')}


def main():
    parser = argparse.ArgumentParser(
        description='check status of raid card', parents=[uoeparser])
    parser.add_argument('-p', '--program', type=Path, metavar='PATH',
                        default="/opt/MegaRAID/MegaCli/MegaCli64",
                        help="name of MegaCli binary")
    args = parser.parse_args()

    metrics = MegaCLI(args)

    if not args.program.exists():
        metrics.log.error(f'binary {args.program} does not exist')
        sys.exit(1)

    try:
        cmd = subprocess.run([args.program, "-PdList", "-aAll", "-nolog"],
                             text=True, capture_output=True, check=True)
    except subprocess.CalledProcessError as e:
        metrics.log.error(e)
        sys.exit(1)

    for line in cmd.stdout.split('\n'):
        # handle adapter line
        if line.startswith('Adapter'):
            _, adapter = line.split('#')
            continue
        try:
            key, value = line.split(':')
        except ValueError:
            continue
        key = key.strip()
        value = value.strip()
        if key == 'Enclosure Device ID':
            enclosure = value
            continue
        if key == 'Slot Number':
            slot_num = value
            continue
        if key == 'Media Error Count':
            key = 'media_error'
            value = int(value)
        elif key == 'Other Error Count':
            key = 'other_error'
            value = int(value)
        elif key == 'Predictive Failure Count':
            key = 'predictive_failure'
            value = int(value)
        elif key == 'Firmware state':
            key = 'firmware'
            if value in [
                    'Unconfigured', 'Hotspare', 'Online, Spun Up', 'JBOD']:
                value = 0
            else:
                value = 1
        elif key == 'Drive has flagged a S.M.A.R.T alert':
            key = 'smart'
            if value == 'No':
                value = 0
            else:
                value = 1
        else:
            continue
        metrics.add(
            key, value, adapter=adapter, enclosure=enclosure, slot=slot_num)

    metrics.close()


if __name__ == '__main__':
    main()
