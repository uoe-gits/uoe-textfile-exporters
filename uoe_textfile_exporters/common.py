__all__ = ['uoeparser', 'Metrics']

import logging
import logging.handlers
import argparse
from pathlib import Path
from io import StringIO
import tempfile


uoeparser = argparse.ArgumentParser(add_help=False)
uoeparser.add_argument('-o', '--output', type=Path, metavar='FILE',
                       help="write results to FILE")
group = uoeparser.add_argument_group('logging options')
group.add_argument("--verbose", action="store_true", default=False,
                   help="Display verbose descriptions of actions taken.")
group.add_argument("--quiet", action="store_true", default=False,
                   help="only display warnings or errors")
group.add_argument("--syslog", action="store_true", default=False,
                   help="Send logging information to syslog")


class Metrics:
    name = ''
    metrics = None

    def __init__(self, cmdargs):
        # sort out logging
        self._log = logging.getLogger(self.name)
        self.log.setLevel(logging.DEBUG)
        # Set a default level for the stderr logging
        level = logging.INFO
        if cmdargs.verbose:
            level = logging.DEBUG
        if cmdargs.quiet:
            level = logging.WARNING

        formatter = logging.Formatter('%(name)s - %(levelname)s: %(message)s')
        if cmdargs.syslog:
            syslog = logging.handlers.SysLogHandler(address='/dev/log')
            syslog.setFormatter(formatter)
            syslog.setLevel(logging.INFO)
            self.log.addHandler(syslog)

        errout = logging.StreamHandler()
        errout.setFormatter(formatter)
        errout.setLevel(level)
        self.log.addHandler(errout)

        if self.metrics is None:
            raise NotImplementedError("need to specify metrics")
        self._output = cmdargs.output
        if self._output is None:
            self._stream = StringIO()
        else:
            self.log.debug(f'storing metrics in {self._output}')
            if not self._output.parent.exists():
                msg = f'output directory {self._output.parent} does not exist'
                self.log.error(msg)
                raise RuntimeError(msg)
            self._stream = tempfile.NamedTemporaryFile(
                mode='w', prefix=str(self._output.absolute()), delete=False)

        for m in self.metrics:
            self._stream.write(
                f"# HELP {self.name}_{m} {self.metrics[m][0]}\n")
            self._stream.write(
                f"# TYPE {self.name}_{m} {self.metrics[m][1]}\n")

    @property
    def log(self):
        return self._log

    def add(self, metric, value, **kwrds):
        if metric not in self.metrics:
            msg = f'no such metric {metric}'
            self.log.error(msg)
            raise LookupError(msg)
        labels = []
        for l, v in kwrds.items():
            labels.append(f'{l}="{v}"')
        labels = ', '.join(labels)
        self._stream.write(f'{self.name}_{metric}{{{labels}}} {value}\n')

    def close(self):
        if self._output is None:
            print(self._stream.getvalue().strip())
        else:
            self.log.debug(f'closing output {self._output}')
            self._stream.close()
            self._stream = Path(self._stream.name)
            self._stream.chmod(0o644)
            self._stream.rename(self._output)
