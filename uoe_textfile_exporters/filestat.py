import argparse
from pathlib import Path
import time

from .common import Metrics
from .common import uoeparser


class FileStat(Metrics):
    name = 'node_file'
    metrics = {'size_bytes': ('Size of a particular file', 'gauge'),
               'age_seconds': ('Age of a particular file', 'gauge')}


def main():
    parser = argparse.ArgumentParser(
        description='get file size and age', parents=[uoeparser])
    parser.add_argument('name', type=Path, nargs='*',
                        help="the file to check")
    parser.add_argument('-w', '--with-file-size', action='store_true',
                        default=False, help="also report filesize")
    args = parser.parse_args()

    metrics = FileStat(args)

    for p in set(args.name):
        if not p.exists():
            # file does not exist set age to 10 days
            metrics.debug(f'file {p} does not exist, setting age to 10 days')
            age = 3600 * 24 * 10
            size = -1
        elif p.is_dir():
            # get the newest file
            age = 3600 * 24 * 10
            size = -1
            for child in p.iterdir():
                if child.is_file():
                    a = time.time() - child.stat().st_mtime
                    if a < age:
                        age = a
                        size = child.stat().st_size
        else:
            age = time.time() - p.stat().st_mtime
            size = p.stat().st_size

        if args.with_file_size:
            metrics.add('size_bytes', size, file=p)
        metrics.add('age_seconds', age, file=p)

    metrics.close()


if __name__ == '__main__':
    main()
