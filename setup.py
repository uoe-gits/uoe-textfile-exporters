#!/usr/bin/env python3

from setuptools import setup
from setuptools import find_packages

release = '0.1.0'
author = 'Magnus Hagdorn'

setup(name='uoe_textfile_exporters',
      packages=find_packages(),
      version=release,
      author=author,
      description='various prometheus textfile exporters',
      url="https://github.com/UoE-Geos/uoe-textfile-exporters",
      entry_points={
          'console_scripts': [
              'uoe-prom-megacli = uoe_textfile_exporters.megacli_check:main',
              'uoe-prom-mvcli = uoe_textfile_exporters.mvcli_check:main',
              'uoe-prom-filestat = uoe_textfile_exporters.filestat:main',
          ],
      },
      )
