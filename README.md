# uoe-textfile-exporters
A collection of prometheus metric exporters using the the node textfile exporter.

## uoe-prom-filestat
Script that checks file age and time and optionally stores it in file. The file is atomically written. If the path is a directory use the newest file in the directory.

## uoe-prom-megacli
Check the status of mega raids using megacli tool.

## uoe-prom-mvcli
Check the status of Dell Boss Cards using mvcli tool. Return 1 when an error is reported, otherwise 0.

